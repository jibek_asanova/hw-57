import React from 'react';
import './IndividualForm.css';

const IndividualForm = (props) => {
    return (
        <form className="IndividualForm">
            <h4>Каждому индивидуально</h4>
            {props.people.map(person => {
                return (
                    <div key={person.id}>
                        <input type="text"
                               name="name"
                               placeholder="Имя"
                               value={person.name}
                               onChange={e => props.changPersonField(person.id, 'name', e.target.value)}
                        />
                        <input type="text"
                               name="price"
                               placeholder="Сумма"
                               value={person.price}
                               onChange={e => props.changPersonField(person.id, 'price', e.target.value)}
                        />
                        <button type="button" className="btn deleteButton" onClick={() => props.deletePerson(person.id)}>Удалить</button>
                    </div>
                )
            })}
            <button type="button"  className="btn addButton" onClick={props.addPerson}>Добавить</button><br/>
            <label>Процент чаевых:</label>
            <input type="text"
                   name="tips"
                   value={props.individualData.tips}
                   onChange={props.getDataIndividual}
            /> % <br/>
            <label>Доставка:</label>
            <input type="text"
                   name="delivery"
                   value={props.individualData.delivery}
                   onChange={props.getDataIndividual}
            /> сом <br/>
            <button type="button" className="btn" onClick={props.getTotalPriceIndividual}>Рассчитать</button>
            <p><strong>Общая сумма: {props.getTotalIndividual}</strong> </p>
            <div>{props.getTotal}</div>

        </form>
    );
};

export default IndividualForm;