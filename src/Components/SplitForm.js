import React from 'react';
import './SplitForm.css';

const SplitForm = ({data, getData, getTotalPrice, totalPrice}) => {
    return (
        <form className="SplitForm">
            <h4>Поровну между участниками</h4>
            <label>Человек:</label>
            <input type="text"
                   name="name"
                   value={data.name}
                   onChange={getData}

            /> чел.<br/>
            <label>Cумма заказа:</label>
            <input type="text"
                   name="price"
                   value={data.price}
                   onChange={getData}
            /> сом <br/>
            <label>Процент чаевых:</label>
            <input type="text"
                   name="tips"
                   value={data.tips}
                   onChange={getData}
            /> % <br/>
            <label>Доставка:</label>
            <input type="text"
                   name="delivery"
                   value={data.delivery}
                   onChange={getData}
            /> сом <br/>
            <button type="button" className="btn" onClick={getTotalPrice}>Рассчитать</button>

            <div>
                <p><strong>Общая сумма: {totalPrice}</strong> </p>
                <p>Количство человек: {data.name}</p>
                <p>Каждый платит: {Math.ceil(data.tips && totalPrice / parseInt(data.name))}</p>
            </div>
        </form>
    );
};

export default SplitForm;