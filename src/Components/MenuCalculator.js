import React, {useState} from 'react';
import './MenuCalculator.css';
import SplitForm from "./SplitForm";
import IndividualForm from "./IndividualForm";

const MenuCalculator = () => {
    const [mode, setMode] = useState('split');
    const [people, setPeople] = useState([]);
    const [data, setData] = useState({
        name: '', price: '', tips: '', delivery: ''
    });
    const [totalPrice, setTotalPrice] = useState(0);
    const [individualData, setIndividualData] = useState({
        tips: '', delivery: ''
    });
    const [totalPrice2, setTotalPrice2] = useState(0);
    const [totalPriceIndividual, setTotalPriceIndividual] = useState([]);

    const onRadioChange = e => {
        setMode(e.target.value);
    };

    const addPerson = () => {
        setPeople(people => [...people, {name: '', price: '', id: Math.random()}])
    };


    const changPersonField = (id, name, value) => {
        setPeople(people => {
            return people.map(person => {
                if(person.id === id) {
                    return {...person, [name] : value}
                }
                return person;
            })
        })
    };

    const deletePerson = id => {
        let peopleCopy = [...people];
        peopleCopy = peopleCopy.filter(p => p.id !== id)
        setPeople(peopleCopy);
        getTotalPriceIndividual(peopleCopy);
    };

    const getDataSplit = e => {
        const {name, value} = e.target
        setData({...data, [name] : value});
    };

    const getDataIndividual = e => {
        const {name, value} = e.target
        setIndividualData({...individualData, [name] : value});
    };

    const getTotalPriceSplit = () => {
        const tips = parseInt(data.price) / 100 * parseInt(data.tips);
        let totalResult;
        if(data.delivery) {
            totalResult = parseInt(data.price) + tips + parseInt(data.delivery);
        } else {
            totalResult = parseInt(data.price) + tips;
        }
        setTotalPrice(totalResult);
    };



    const getTotalPriceIndividual = (people) => {
        let total = people.reduce((prev, next) => prev + parseInt(next.price), 0);

        if(individualData.delivery) {
            setTotalPrice2(total + Math.ceil(total / 100 * parseInt(individualData.tips) + parseInt(individualData.delivery)));
            const delivery = individualData.delivery / people.length;
            setTotalPriceIndividual(people.map((item, index) => (
                <div key={index}>
                    <p>{item.name} <span>{parseInt(item.price) + Math.ceil(parseInt(item.price) / 100 * individualData.tips) + delivery}</span> som</p>
                </div>
            )))
        } else {
            setTotalPrice2(total + Math.ceil(total / 100 * parseInt(individualData.tips)));
            setTotalPriceIndividual(people.map((item, index) => (
                <div key={index}>
                    <p>{item.name} <span>{parseInt(item.price) + Math.ceil(parseInt(item.price) / 100 * individualData.tips)}</span> som</p>
                </div>
            )))
        }
    };



    return (
        <div className="MainBlock">
            <div className="CheckBoxes">
                <p>Сумма заказа считается:</p>
                <p>
                    <label>
                        <input type="radio"
                               checked={mode === 'split'}
                               name="mode"
                               value="split"
                               onChange={onRadioChange}
                        />{' '}
                        Поровну между участниками
                    </label>
                </p>
                <p>
                    <label>
                        <input type="radio"
                               checked={mode === 'individual'}
                               name="mode"
                               value="individual"
                               onChange={onRadioChange}
                        />{' '}
                        Каждому индивидуально
                    </label>
                </p>
            </div>

            <div>
                {mode === 'split' ? (
                    <SplitForm
                        data={data}
                        getData={getDataSplit}
                        getTotalPrice={getTotalPriceSplit}
                        totalPrice={totalPrice}
                    />
                ) : (
                    <IndividualForm
                        people={people}
                        changPersonField={changPersonField}
                        addPerson={addPerson}
                        deletePerson={deletePerson}
                        individualData={individualData}
                        getDataIndividual={getDataIndividual}
                        getTotalPriceIndividual={() => getTotalPriceIndividual(people)}
                        getTotalIndividual={totalPrice2}
                        getTotal={totalPriceIndividual}
                    />
                )}

            </div>
        </div>
    );
};

export default MenuCalculator;